from typing import List

import altair as alt
import pandas as pd
import plotly.figure_factory as ff
import streamlit as st
from snowflake.snowpark.context import get_active_session

# Get the current credentials
session = get_active_session()

# basic info
database = "PC_DBT_DB"
schema = "DBT_MPHAM"


@st.cache_data()
def query_data(sql):
    data = session.sql(sql)
    return data.to_pandas()


def show_hospitals(
        df: pd.DataFrame,
        cluster_table: str,
        cluster_colname: str,
        ordered_measures: List[str],
        limit=20
):
    cluster_option = st.selectbox(
        label='Select cluster to show top hospitals in this cluster',
        options=df[cluster_colname].unique()
    )

    measures = ', '.join(ordered_measures)
    orderby_measures = ', '.join([m + ' DESC' for m in ordered_measures])

    df_hospitals = query_data(f"""
        SELECT
            facility.*,
            {measures}
        FROM 
            {database}.{schema}.{cluster_table} as clusters
            INNER JOIN {database}.{schema}.FACILITY_DIMENSION as facility
            ON clusters.FACILITY_ID = facility.FACILITY_ID
        WHERE 
            "{cluster_colname}" = {"'" + cluster_option + "'" if isinstance(cluster_option, str)
    else cluster_option}
        ORDER BY {orderby_measures} 
        LIMIT {limit}
    """)
    st.dataframe(df_hospitals, use_container_width=True)


def scatter_plot(df, x, y, cluster_colname='cluster'):
    st.altair_chart(
        altair_chart=alt.Chart(df).mark_circle().encode(
            x=x,
            y=y,
            color=alt.Color(f'{cluster_colname}:N', scale=alt.Scale(scheme='category10'))
        ).properties(
            width=500,
            height=550
        ),
        use_container_width=True,
        theme='streamlit'
    )


def displot(df, measure, cluster_colname='cluster'):
    data = [df[df[cluster_colname] == c][measure].to_numpy() for c in df[cluster_colname].unique()]
    labels = [str(c) for c in df[cluster_colname].unique()]
    st.plotly_chart(
        ff.create_distplot(
            data,
            labels,
            bin_size=1
        ).update_layout(title_text=f'Displot of clusters based on measure {measure}'),
        theme="streamlit",
        use_container_width=True
    )

