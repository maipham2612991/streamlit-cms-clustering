# Import python packages

from common_utils import *


@st.cache_data()
def _data_overview_df(df):
    """ Return displayed overview data"""

    df_ov = pd.DataFrame(index=df.columns)
    df_ov['total'] = df.shape[0]
    df_ov['pdtype'] = df.dtypes
    df_ov['unique_count'] = df.nunique()
    df_ov['%-uniques'] = round(df_ov['unique_count']/df_ov['total'] * 100, 2).astype(str) + '%'
    df_ov['dup_count'] = df_ov['total'] - df_ov['unique_count']
    df_ov['%-duplicates'] = round(df_ov['dup_count']/df_ov['total'] * 100, 2).astype(str) + '%'
    df_ov['null_count'] = df.isnull().sum()
    df_ov['%-null'] = round(df_ov['null_count']/df_ov['total'] * 100, 2).astype(str) + '%'
    df_ov['blank_count'] = (df == '').sum()
    df_ov['%-blanks'] = round(df_ov['blank_count']/df_ov['total'] * 100, 2).astype(str) + '%'
    df_ov['zero_count'] = (df == 0).sum()
    df_ov['%-zeros'] = round(df_ov['zero_count']/df_ov['total'] * 100, 2).astype(str) + '%'

    return df_ov


def data_overview():
    """Data overview table"""
    df = query_data(f"""
        SELECT *
        FROM {database}.{schema}.FACILITY_MEASURE_SCORE_PIVOT
    """)

    st.subheader("Data Overview")
    st.write("""
        Data overview of all healthcare measures. There are total 4701 hospitals.  
    """)
    df_ov = _data_overview_df(df)
    st.dataframe(df_ov.style.background_gradient(), use_container_width=True)

    st.subheader("Data Sample")
    st.dataframe(df.head(5), use_container_width=True)


def colonoscopy_care():
    cluster_table = 'FACILITY_COLONOSCOPY_CARE_CLUSTERING'
    df = query_data(f"""
        SELECT
            CLUSTER,
            COUNT(*) as TOTAL_HOSPITAL
        FROM {database}.{schema}.{cluster_table}
        GROUP BY 1
    """)
    st.bar_chart(data=df, x='CLUSTER', y='TOTAL_HOSPITAL')

    show_hospitals(
        df=df,
        cluster_table=cluster_table,
        cluster_colname='CLUSTER',
        ordered_measures=['OP_29']
    )


def _displot_measures(df, measures):
    for i in range(0, len(measures)-1, 2):
        col1, col2 = st.columns(2)
        with col1:
            displot(df, measures[i])
        with col2:
            displot(df, measures[i+1])

    if len(measures) % 2 == 1:
        displot(df, measures[-1])


def healthcare_vaccination():
    cluster_table = 'FACILITY_VACCINATION_CLUSTERING'
    df = query_data(f"""
        SELECT *
        FROM {database}.{schema}.{cluster_table}
    """)

    scatter_plot(df, x='HCP_COVID_19', y='IMM_3')

    measures = ['IMM_3', 'HCP_COVID_19']
    _displot_measures(df, measures)
    
    show_hospitals(
        df=df,
        cluster_table=cluster_table,
        cluster_colname='cluster',
        ordered_measures=measures
    )
        

def sepsis_care():
    cluster_table = 'FACILITY_SEPSIS_CARE_CLUSTERING'
    df = query_data(f"""
        SELECT *
        FROM {database}.{schema}.{cluster_table}
    """)
    scatter_plot(df, x='PC1', y='PC2')

    measures = ['SEP_SH_3HR', 'SEP_SH_6HR', 'SEV_SEP_3HR', 'SEV_SEP_6HR', 'SEP_1']
    _displot_measures(df, measures)

    show_hospitals(
        df=df,
        cluster_table=cluster_table,
        cluster_colname='cluster',
        ordered_measures=measures
    )


def electronic_clinical():
    cluster_table = 'FACILITY_ELECTRONIC_CLINIC_CLUSTERING'
    df = query_data(f"""
        SELECT *
        FROM {database}.{schema}.{cluster_table}
    """)

    scatter_plot(df, x='PC1', y='PC2')
    
    measures = ['ED_2_STRATA_1', 'ED_2_STRATA_2', 'SAFE_USE_OF_OPIOIDS', 'STK_02', 'STK_05', 'STK_06', 'VTE_1', 'VTE_2']
    _displot_measures(df, measures)


def emergency_department():
    cluster_table = 'FACILITY_EMERGENCY_CLUSTERING'
    df = query_data(f"""
        SELECT *
        FROM {database}.{schema}.{cluster_table}
    """)

    scatter_plot(df, x='PC1', y='PC2')

    measures = ['OP_18B', 'EDV_encoded', 'OP_18C', 'OP_22', 'OP_23']
    _displot_measures(df, measures)


def cataract_outcome():
    cluster_table = 'FACILITY_CATARACT_CLUSTERING'
    df = query_data(f"""
        SELECT
            CLUSTER,
            COUNT(*) as TOTAL_HOSPITAL
        FROM {database}.{schema}.{cluster_table}
        GROUP BY 1
    """)
    st.bar_chart(data=df, x='CLUSTER', y='TOTAL_HOSPITAL')

    show_hospitals(
        df=df,
        cluster_table=cluster_table,
        cluster_colname='CLUSTER',
        ordered_measures=['OP_31']
    )
    
    
def heart_attack():
    cluster_table = 'FACILITY_HEART_ATTACK_CLUSTERING'
    df = query_data(f"""
        SELECT *
        FROM {database}.{schema}.{cluster_table}
    """)

    scatter_plot(df, x='OP_3B', y='OP_2', cluster_colname='CLUSTER')

    show_hospitals(
        df=df,
        cluster_table=cluster_table,
        cluster_colname='CLUSTER',
        ordered_measures=['OP_3B', 'OP_2']
    )
    

def data_clustering():
    st.subheader("Hospital Clustering")

    condition_mapping = {
        'Colonoscopy care': colonoscopy_care,
        'Healthcare Personnel Vaccination': healthcare_vaccination,
        'Sepsis Care': sepsis_care,
        'Electronic Clinical Quality Measure': electronic_clinical,
        'Emergency Department': emergency_department,
        'Cataract surgery outcome': cataract_outcome,
        'Heart Attack or Chest Pain': heart_attack,
    }

    option = st.selectbox(
        label='Select condition to see clusters',
        options=condition_mapping.keys()
    )

    condition_mapping[option]()


if __name__ == '__main__':

    # App layout and header
    st.set_page_config(layout="wide")
    st.title("Hospital Clustering Overview :hospital:")
    st.write(
        """This is the dashboard of timely and effective care hospitals clustering. There are 25 measures belonged to 
        7 conditions. The hospitals will be clustered based on each condition and the measure scores of that condition.
        Check out details of data at https://d1ux3ufcwoooy2.cloudfront.net/.
        """
    )
    
    # Start content
    data_overview()
    data_clustering()
